package com.frezneel.SpringBlog.users.services.impl;

import com.frezneel.SpringBlog.users.entites.User;
import com.frezneel.SpringBlog.users.repository.UserRepo;
import com.frezneel.SpringBlog.users.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    @Override
    public Optional<User> getUserById(Long id) {
        return userRepo.findById(id);
    }
}
