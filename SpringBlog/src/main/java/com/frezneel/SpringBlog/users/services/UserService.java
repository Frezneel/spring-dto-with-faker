package com.frezneel.SpringBlog.users.services;

import com.frezneel.SpringBlog.users.entites.User;
import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers();

    Optional<User> getUserById(Long id);
}