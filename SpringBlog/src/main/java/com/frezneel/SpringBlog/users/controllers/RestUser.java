package com.frezneel.SpringBlog.users.controllers;

import com.frezneel.SpringBlog.users.dtos.responses.UserResponse;
import com.frezneel.SpringBlog.users.entites.User;
import com.frezneel.SpringBlog.users.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/users")
public class RestUser {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public ResponseEntity<?> getAllUsers(){
        Map<String, Object> result = new HashMap<>();
        try {
            List<User> users = userService.getAllUsers();
            List<UserResponse> userResponse = users.stream()
                    .map(user -> {
                        UserResponse response = new UserResponse();
                        response.setName(user.getName());
                        response.setEmail(user.getEmail());
                        return response;
                    }).collect(Collectors.toList());
            result.put("status", "200");
            result.put("message", "Data Successfully Acquiring");
            result.put("data", userResponse);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }catch (Exception e){
            result.put("status", "500");
            result.put("message", "Failed Acquiring Data");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
