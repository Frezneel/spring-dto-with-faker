package com.frezneel.SpringBlog;

import com.frezneel.SpringBlog.config.BCryptConfig;
import com.frezneel.SpringBlog.users.entites.User;
import com.frezneel.SpringBlog.users.repository.UserRepo;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Locale;

@SpringBootApplication
public class SpringBlogApplication {

	@Autowired
 	private UserRepo userRepo;
	@Autowired
	private BCryptConfig bCryptConfig;

	public static void main(String[] args) {
		SpringApplication.run(SpringBlogApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(){
		return args -> {
			User user1 = new User("Galih Muhammad Ichsan", "galih@gmail.com", bCryptConfig.EncodePassword("apaaja12"));
			userRepo.save(user1);
			Faker faker = new Faker(new Locale("en-US"));
			for (int i = 0; i < 5; i++) {
				User user = new User(faker.name().fullName(), faker.internet().safeEmailAddress(), bCryptConfig.EncodePassword(faker.internet().password()));
				userRepo.save(user);
			}
		};
	}

}
